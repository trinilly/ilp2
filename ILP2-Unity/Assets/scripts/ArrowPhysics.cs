﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowPhysics : MonoBehaviour
{

    public float speed = 15f;
    public Rigidbody2D rigidbody;

    // Start is called before the first frame update
    void Start()
    {
        //shoot in fornt of the player
        rigidbody.velocity = transform.right * speed;
    }

  
}
