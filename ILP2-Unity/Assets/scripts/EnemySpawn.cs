﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public GameObject enemyPrefab;
    public int spawnRate;

    void Start()
    {
        //continually spawn enemies at time intervals
        InvokeRepeating("Spawn", spawnRate, spawnRate);
    }

   void Spawn()
    {
        // spawn enemy prefab at the spawner
        Instantiate(enemyPrefab, transform.position, transform.rotation);
    }
}

