﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public float smooth;

    public Vector3 offset;

   

    private void LateUpdate()
    {
        //camera follows the player with delay
        Vector3 finalPosition = target.position + offset;
        Vector3 smoothed = Vector3.Lerp(transform.position, finalPosition, smooth);

        transform.position = smoothed;
    }
  

}
