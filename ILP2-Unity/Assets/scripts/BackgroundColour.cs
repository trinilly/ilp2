﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundColour : MonoBehaviour
{
    SpriteRenderer spriteRendererComponent;

    Color first;
    Color end;

    public AnimationCurve curve;

    float start;
    public float length;
  
    // Start is called before the first frame update
    void Awake()
    {
        first = new Color32(26, 43, 19, 255);
    

        end = new Color32(41, 82, 25, 255);

        spriteRendererComponent = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        //shift between first and last colours
        float timeDecimal = (Time.time - start) / length;
        spriteRendererComponent.color = Color.Lerp(first, end, curve.Evaluate(timeDecimal));
    }
}
