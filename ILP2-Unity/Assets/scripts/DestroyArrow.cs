﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyArrow : MonoBehaviour
{
    

    // Update is called once per frame
    void Update()
    {
        // destroy arrows after 2 seconds
        Destroy(gameObject, 2f);
    }
}
