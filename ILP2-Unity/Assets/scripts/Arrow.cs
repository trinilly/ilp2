﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    public Transform ShootFromHere;
    public GameObject arrowPrefab;

    // Update is called once per frame
    void Update()
    {
        //shot when right mouse button pressed
        if (Input.GetMouseButtonUp(1))
        {
            shoot();
        }
    }

    void shoot()
    {
        // create arrows from game object
        Instantiate(arrowPrefab, ShootFromHere.position, ShootFromHere.rotation);

        
    }
}
