﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Life : MonoBehaviour
{
    int health;
    public Text healthText;


    private void Start()
    {
        health = 5;
    }
    private void Update()
    {
        healthText.text = "HEALTH : " + health;

        

        if (health <= 0)
        {
            //game over when lives run out
            SceneManager.LoadScene("menu", LoadSceneMode.Single);
        }
    }

    public void PlayerHit(float damage)
    {
        // lose a life when hit by enemy
        health--;
    }
}
