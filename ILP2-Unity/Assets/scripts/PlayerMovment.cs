﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovment : MonoBehaviour
{
    public float speed;
    bool move;

    Vector2 moveTo;

    Vector3 mousePosition;

    public Animator anim;

    Rigidbody2D rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    void Start()
    {
        move = false;
    }

    void Update()

    {
        //move player towards where mouse is pressed
        FollowMouse();

        //face the mouse
        FaceMouse();

        //jump with scroll
        jump();

        //play walk animation when moving
        anim.SetFloat("MoveSpeed", rb.velocity.magnitude);
    }

    void FaceMouse()
    {

        //find mouse position
        Vector3 mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);



        // find where to face the object
        Vector3 deltaPos = (Vector2)mousePosition - (Vector2)transform.position;
        float angle = Mathf.Atan2(deltaPos.y, deltaPos.x);

        //rotate objece to angle
        transform.rotation = Quaternion.Euler(0, 0, angle * Mathf.Rad2Deg);

    }

    void FollowMouse()
    {
        // move toward the mouse when pressed 
        if (Input.GetMouseButton(0))
        {
            moveTo = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            move = true;

        }

        //stop moving when mouse is not pressed
        else if (!Input.GetMouseButton(0))
        {
            move = false;
        }

        if (move == true)
        {
            //move towards mouse
            Vector2 moveDirection = (moveTo - rb.position).normalized;
            rb.velocity = moveDirection * speed * Time.deltaTime;
        }
        else if (move == false)
        {
            //stop moving
            rb.velocity = Vector3.zero;
        }

    }

    void jump()
    {
        //jump with scroll
        if (Input.GetMouseButtonDown(2))
        {
            rb.AddForce(new Vector2(0, 50), ForceMode2D.Impulse);
            //play jump animation when scroll is pressed
            anim.SetTrigger("Jump");
        }
    }


    void PlayerHit()
    {
        // play hurt animation when hit by enemy
        anim.SetTrigger("PlayerHit");
    }
}


