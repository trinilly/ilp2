﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed;

    public GameObject playerObject;
    public GameObject score;
    public GameObject lives;
    public Transform player;

    private void Start()
    {
        player = GameObject.Find("player").transform;
        lives = GameObject.Find("HEALTH");
        score = GameObject.Find("SCORE");
        playerObject = GameObject.Find("player");
    }

    // Update is called once per frame
    void Update()
    {
        // move towards player
        transform.position = Vector3.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
           //send mesage to other scripts when enemy hits player
            lives.SendMessage("PlayerHit", 1);
            playerObject.SendMessage("PlayerHit", 1);

            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "arrow")
        {

            //send message to other scripts when arrow hits enemy
            score.SendMessage("EnemyKilled");
            Destroy(gameObject);
        }
    }

}
